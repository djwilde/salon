package main

import (
	"bitbucket.org/djwilde/salon/event"
)

type Aggregate struct {
	event.Aggregate
	Info
}

type Info struct {
	Name     string
	Approved bool
}

func (salon *Aggregate) Init(name string) {
	salon.Apply(Created{salon.Id, name})
}

func (salon *Aggregate) Apply(ev interface{}) {
	salon.Record(ev)
	switch ev := ev.(type) {
	case Created:
		salon.Name = ev.Name
	case Approved:
		salon.Approved = true
	}
}
