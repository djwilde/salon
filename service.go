package main

import (
	"bitbucket.org/djwilde/salon/event"
)

type Service struct {
	bus   event.Bus
	store event.Store

	repo *Repository
}

func NewService(bus event.Bus, store event.Store) *Service {
	return &Service{
		bus:   bus,
		store: store,

		repo: &Repository{store},
	}
}

func (srv *Service) NewSalon(name string) (event.AggregateId, error) {
	salon := srv.repo.Create()
	salon.Init(name)
	return salon.Id, srv.repo.SaveChanges(salon)
}
