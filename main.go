package main

import (
	"fmt"
	"net/http"
	"net/url"

	"bitbucket.org/djwilde/salon/event"
	"bitbucket.org/djwilde/salon/event/membus"
	"bitbucket.org/djwilde/salon/event/memstore"
)

var (
	bus   event.Bus
	store event.Store

	service *Service
)

func main() {
	bus = membus.New()
	store = memstore.New(bus)

	service = NewService(bus, store)

	http.HandleFunc("/get", get)
	http.HandleFunc("/create", create)
	http.ListenAndServe(":3100", nil)
}

func get(w http.ResponseWriter, r *http.Request) {
}

func create(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		values, err := url.ParseQuery(r.URL.RawQuery)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Error:", err)
			return
		}
		if len(values.Get("name")) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Error:", "No name provided")
			return
		}

		addedSalonID, err := service.NewSalon(values.Get("name"))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Error:", err)
			return
		}
		w.WriteHeader(http.StatusCreated)
		fmt.Fprint(w, addedSalonID)
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Error: Only POST accepted")
	}
}
