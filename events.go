package main

import (
	"encoding/gob"

	"bitbucket.org/djwilde/salon/event"
)

type Created struct {
	ID   event.AggregateId
	Name string
}

type Approved struct {
	Id event.AggregateId
}

func init() {
	gob.Register(Created{})
}
