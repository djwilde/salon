package main

import "bitbucket.org/djwilde/salon/event"

/**
 * Repository
 */
type Repository struct {
	event.Store
}

func (repo *Repository) Create() *Aggregate {
	salon := &Aggregate{}
	salon.Id = event.GenerateId()
	return salon
}

func (repo *Repository) ById(id event.AggregateId) (salon *Aggregate, ok bool) {
	events, ok := repo.Store.List(id)
	if !ok {
		return nil, false
	}
	salon = &Aggregate{}
	salon.Id = id
	for _, event := range events {
		salon.Version = event.Version
		salon.Apply(event.Data)
	}
	salon.Changes = nil
	return salon, true
}
